
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {SettingsService} from "../../settings/settings.service";



//public this.ServerUrl = "";//http://www.tapper.co.il/salecar/laravel/public/api/";
@Injectable()

export class KitchenService
{
    public ServerUrl = "";
    public headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    public options = new RequestOptions({headers:this.headers});

    public Items:any[]=[];
    public Kitchens:any[]=[];

    constructor(private http:Http ,  settings:SettingsService)
    {
        this.ServerUrl = settings.ServerUrl;
    };

    GetAllItems(url:string)
    {
        let body = new FormData();
        body.append('uid', window.localStorage.identify);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.Items = data }).toPromise();
    }

    GetAllKitchens(url:string)
    {
        let body = new FormData();
        body.append('uid', window.localStorage.identify);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.Kitchens = data }).toPromise();
    }

    AddItem(url,Items)
    {
        this.Items = Items;
        let body = 'items=' + JSON.stringify(this.Items);
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(res => res).do((data)=>{}).toPromise();
    }

    EditItem(url,Items)
    {
        this.Items = Items;
        let body = 'items=' + JSON.stringify(this.Items);
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(res => res.json()).do((data)=>{}).toPromise();
    }

    DeleteCompany(url,Id)
    {
        let body = 'id=' + Id;
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(res => res.json()).do((data)=>{}).toPromise();
    }
};


