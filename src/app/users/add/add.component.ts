import {Component, OnInit, ElementRef, ChangeDetectionStrategy , ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormControl , FormGroup, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";

import { FileUploader, FileUploaderOptions } from 'ng2-file-upload/ng2-file-upload';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit {

    public navigateTo:string = '/users/index';
    public paramsSub;
    public id: number;
    public imageSrc: string = '';
    registerForm: FormGroup;
    public folderName:string = 'users';
    public rowsNames:any[] = ['שם מלא','כתובת','טלפון','מייל','סיסמה','מספר חשבון','שם הבנק','מספר סניף','שם בעל חשבון הבנק'];
    public rows:any[] = ['name','address','phone','mail','password','bank_acoount_number','bank_number','snif_number','accout_name'];
    public customer_id:any;

    @ViewChild("fileInput") fileInput;

    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router) {

        this.route.queryParams.subscribe(params => {
            this.customer_id = params['customer_id'];
        });


        console.log("Row : " , this.rows)
    }

    onSubmit(form:NgForm)
    {

        //console.log(form.value);
        //let fi = this.fileInput.nativeElement;
        let fileToUpload;
        //if (fi.files && fi.files[0]) {fileToUpload = fi.files[0];}

        this.service.AddItem('addUser',form.value,fileToUpload).then((data: any) => {
            console.log("addUser : " , data);
            this.router.navigate(['/','users','index'], { queryParams: { customer_id: this.customer_id} });
        });
    }
    
    ngOnInit() {

        this.paramsSub = this.route.params.subscribe(params => this.id = params['id']);

        this.registerForm = new FormGroup({
            'name':new FormControl(null,Validators.required),
            'address':new FormControl(null,Validators.required),
            'phone':new FormControl(null,[Validators.required, Validators.minLength(9),Validators.required,Validators.pattern('^[0-9]+$')]),
            'mail':new FormControl(null,[Validators.required, Validators.email]),
            'password':new FormControl(null,Validators.required),
            'bank_acoount_number': new FormControl(null,[Validators.required,Validators.pattern('^[0-9]+$')]),
            'bank_number': new FormControl(null,[Validators.required,Validators.pattern('^[0-9]+$')]),
            'snif_number': new FormControl(null,[Validators.required,Validators.pattern('^[0-9]+$')]),
            'accout_name':new FormControl(null,Validators.required),
        })
    }
    
    ngOnDestroy() {
        this.paramsSub.unsubscribe();
    }

}
