import {Routes} from '@angular/router';


import {IndexComponent} from "./index/index.component";

export const MaindRoutes: Routes = [{
    path: '',
    children: [{
        path: 'index',
        component: IndexComponent,
        data: {
            heading: 'היסטוריית תשלומים'
        }
    }]
}];
