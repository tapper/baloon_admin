import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Router,ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    public Id;
    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    imagepath: string = '';
    settings = '';
    avatar = '';
    deleteModal: any;
    companyToDelete: any;
    public folderName:string = 'customers_branches';
    public addButton:string = 'הוסף משתמש'


    constructor(public MainService: MainService, private modalService: NgbModal ,settings: SettingsService, private route: ActivatedRoute,
                private router: Router) {

        this.route.queryParams.subscribe(params => {
            this.Id = params['customer_id'];
            this.host = settings.host;
            this.imagepath = settings.imagepath;
        });

        this.getItems();
    }

    ngOnInit() {



    }

    getItems() {
        this.MainService.GetCategories('WebGetCustomersBranches',this.Id).then((data: any) => {
            console.log("WebGetCustomersBranches : ", data) ,
            this.ItemsArray = data,
            this.ItemsArray1 = data

        })
    }

    DeleteItem() {
        this.MainService.DeleteItem('WebDeleteCustomerBranch', this.ItemsArray[this.companyToDelete].id).then((data: any) => {
            this.getItems();
        })
    }


    openDeleteModal(content,index)
    {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    }

    async deleteCompany()
    {
        this.deleteModal.close();
        this.DeleteItem();
        console.log("Company To Delete : " , this.companyToDelete)
    }

    openAddBranchPage()
    {
        this.router.navigate(['/','customers_branches','add'], { queryParams: { customer_id: this.Id } });
    }

    openEditPage(index,id)
    {
        this.router.navigate(['/','customers_branches','edit'], { queryParams: { customer_id: this.Id,id: index } });
    }


    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

}
