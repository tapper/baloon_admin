import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormGroup,FormControl, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
import {ApiService} from '../../_services/api/api.service';


@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './edit.component.css']
})

export class EditComponent  implements OnInit {
    public Id;
    public customer_id;
    public navigateTo:string = '/customers_branches/index';
    public paramsSub;
    public id: number;
    public imageSrc: string = '';
    registerForm: FormGroup;
    public folderName:string = 'customers_branches';
    public rowsNames:any[] = ['כותרת'];
    public rows:any[] = ['name'];
    public latitude:any;
    public longitude: any;
    @ViewChild("fileInput") fileInput;
    @ViewChild("search")
    public searchElementRef: ElementRef;
    errors: Array<any> = [];
    host: string = '';
    imagepath: string = '';
    formattedaddress = '';
    public Item;


    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router, public settings: SettingsService,private mapsAPILoader: MapsAPILoader,
                private ngZone: NgZone,public api: ApiService) {

        this.paramsSub = this.route.queryParams.subscribe(params => {
            this.Id = params['id'];
            this.customer_id = params['customer_id'];
            this.Item = this.service.Items[this.Id];
            this.host = settings.host;
            //this.Image = this.host+''+this.Item.image;
            console.log("fkitchens " ,this.Item )
        });



    }

    onSubmit(form:NgForm) {

        //console.log(form.value);
        //let fi = this.fileInput.nativeElement;
        let fileToUpload;
        //if (fi.files && fi.files[0]) {fileToUpload = fi.files[0];}


        this.errors = [];
        //if (this.latitude  == "" || this.latitude  == undefined)
        //    this.errors.push({message: "יש להזין מיקום מדויק בכתובת"});


        if (this.errors.length == 0) {

            let newaddress = '';

            if (this.latitude == "" || this.latitude == undefined)
                newaddress = form.value.address;
            else
                newaddress = this.formattedaddress;



            let payload: any = {};
            payload.id = this.Item.id;
            payload.name = form.value.name;
            payload.address = newaddress;
            payload.location_lat = this.latitude;
            payload.location_lng = this.longitude;
            console.log(payload);

            this.api.sendPost('WebUpdateCustomerBranch', payload).subscribe(data => {
                this.router.navigate(['/', 'customers_branches', 'index'], {queryParams: {customer_id: this.customer_id}});
            }, error => {
                this.errors.push({message: "שגיאה בעידכון יש לנסות שוב"});
            });
        }
    }

    ngOnInit() {

        this.registerForm = new FormGroup({
            'id':new FormControl(this.Item.id),
            'name':new FormControl(this.Item.name,Validators.required),
            'address':new FormControl(this.Item.address,Validators.required),
        })

        // Load Places Autocomplete
        this.mapsAPILoader.load().then(() => {
            let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
                types: ['address']
            });
            autocomplete.addListener('place_changed', () => {

                this.ngZone.run(() => {
                    // get the place result
                    let selectedPlace = autocomplete.getPlace();
                    console.log("place:" ,selectedPlace )
                    let newaddress = String(selectedPlace.geometry.location);
                    let split = newaddress.split(",");
                    this.latitude = split[0].replace("(", "");
                    this.longitude = split[1].replace(")", "");
                    this.formattedaddress = selectedPlace.formatted_address;;
                });
            });
        });



    }



    ngOnDestroy() {
        this.paramsSub.unsubscribe();
    }

}
