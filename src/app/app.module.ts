import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule, HttpClient} from '@angular/common/http';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {SidebarModule} from 'ng-sidebar';

import {AppRoutes} from './app.routing';
import {AppComponent} from './app.component';
import {AdminLayoutComponent} from './layouts/admin/admin-layout.component';
import {AuthLayoutComponent} from './layouts/auth/auth-layout.component';
import {SharedModule} from './shared/shared.module';
import {SettingsService} from "../settings/settings.service";
import {HttpModule} from "@angular/http";
import {FontawesomeComponent} from "./icons/fontawesome/fontawesome.component";
import {LineaComponent} from "./icons/linea/linea.component";
import {SliComponent} from "./icons/sli/sli.component";
import {ButtonIconsComponent} from "./components/button-icons/button-icons.component";
import {DataTableComponent} from "./datatable/data-table/data-table.component";
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {TableSortingComponent} from "./datatable/table-sorting/table-sorting.component";
import {TableSelectionComponent} from "./datatable/table-selection/table-selection.component";
import {TablePinningComponent} from "./datatable/table-pinning/table-pinning.component";
import {TablePagingComponent} from "./datatable/table-paging/table-paging.component";
import {TableFilterComponent} from "./datatable/table-filter/table-filter.component";
import {TableEditingComponent} from "./datatable/table-editing/table-editing.component";
import {AuthGuard} from "./_guards/auth.guard";
import {ServicesModule} from './_services/services.module';
import {ApiService} from './_services/api/api.service';
import { AgmCoreModule } from '@agm/core';


export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        AppComponent,
        AdminLayoutComponent,
        AuthLayoutComponent,
        FontawesomeComponent,
        SliComponent,
        ButtonIconsComponent,
        LineaComponent,
        DataTableComponent,
        TableEditingComponent,
        TableFilterComponent,
        TablePagingComponent,
        TablePinningComponent,
        TableSelectionComponent,
        TableSortingComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        SharedModule,
        RouterModule.forRoot(AppRoutes),
        FormsModule,
        NgxDatatableModule,
        HttpClientModule,
        HttpModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
        NgbModule.forRoot(),
        SidebarModule.forRoot(),
        ServicesModule,
        AgmCoreModule.forRoot({
            apiKey: "AIzaSyA4uXPidpv7gDsUIXwS30CMQNs5M-t6DOs",
            libraries: ["places"]
        }),
    ],
    providers: [ApiService,SettingsService, AuthGuard],
    bootstrap: [AppComponent]
})
export class AppModule {
}
