import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Router} from "@angular/router";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    settings = '';
    avatar = '';
    imagepath = '';
    deleteModal: any;
    companyToDelete: any;
    public folderName:string = 'customers';
    public addButton:string = 'הוסף לקוח'


    constructor(public MainService: MainService, private modalService: NgbModal ,settings: SettingsService, public router:Router) {
        this.host = settings.host,
        this.avatar = settings.avatar,
        this.imagepath = settings.imagepath
        this.getItems();
    }

    ngOnInit() {
    }


    openBranchesPage(id) {
        this.router.navigate(['/','customers_branches','index'], { queryParams: { customer_id: id } });
    }

    openQuestionsAnswersPage(id) {
        this.router.navigate(['/','customers_questions_answers','index'], { queryParams: { customer_id: id } });
    }

    openUsersPage (id) {
        this.router.navigate(['/','users','index'], { queryParams: { customer_id: id } });
    }

    openRecommendedPage(id) {
        this.router.navigate(['/','recommended','index'], { queryParams: { customer_id: id } });
    }

    openWithdrawPage(id) {
        this.router.navigate(['/','withdrawal','index'], { queryParams: { customer_id: id } });
    }

    openWithdrawHistoryPage(id) {
        this.router.navigate(['/','withdrawal_history','index'], { queryParams: { id: 0,customer_id: id } });
    }




    getItems() {
        this.MainService.GetItems('WebGetCustomers').then((data: any) => {
            console.log("WebGetCustomers : ", data) ,
            this.ItemsArray = data,
            this.ItemsArray1 = data

        })
    }

    DeleteItem() {
        this.MainService.DeleteItem('WebDeleteCustomer', this.ItemsArray[this.companyToDelete].id).then((data: any) => {
            this.getItems();
        })
    }


    openDeleteModal(content,index)
    {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    }

    async deleteCompany()
    {
        this.deleteModal.close();
        this.DeleteItem();
        console.log("Company To Delete : " , this.companyToDelete)
    }



    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

}
