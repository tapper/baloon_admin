import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormGroup,FormControl, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {DomSanitizer} from '@angular/platform-browser';



@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './edit.component.css']
})

export class EditComponent  implements OnInit {
    registerForm: FormGroup;
    public Id;
    public navigateTo:string = '/customers/index';
    public imageSrc: string = '';
    public Items:any[]=[];
    public rowsNames:any[] = ['שם הלקוח','טלפון','כתובת אתר','תיאור קצר','אימייל','סיסמה','כותרת SMS באנגלית בלבד'];
    public rows:any[] = ['name','phone','website','description','email','password','sms_title'];
    public Item;
    public host;
    public Image;
    public Change:boolean = false;
    @ViewChild("fileInput") fileInput;
    logo = {path: null, file: null};
    errors: Array<any> = [];
    imagepath;


    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router, public settings: SettingsService,private sanitizer: DomSanitizer) {
        this.route.params.subscribe(params => {
            this.Id = params['id'];
            this.Item = this.service.Items[this.Id];
            this.host = settings.host;
            this.imagepath = settings.imagepath;
            console.log("fkitchens " ,this.Item )
        });
    }

    onSubmit(form:NgForm)
    {
        
        this.errors = [];

        if (this.logo.file === null && this.Item.image == ''){
            this.errors.push({message: "יש לבחור תמונה"});
            return;
        }

        if (this.errors.length == 0)
        {

            let fi = this.fileInput.nativeElement;
            let fileToUpload;
            if (fi.files && fi.files[0]) {fileToUpload = fi.files[0];}
            let payload: any = {};
            payload.id = this.Item.id;
            payload.name = form.value.name;
            payload.phone = form.value.phone;
            payload.email = form.value.email;
            payload.password = form.value.password;
            payload.website = form.value.website;
            payload.description = form.value.description;
            payload.sms_title = form.value.sms_title;
            payload.image = this.logo.file;

            this.service.AddItem('WebEditCustomer',payload,fileToUpload).then((data: any) => {
                //let response = data.json();
                //console.log("WebAddCustomer : " , response);
                this.router.navigate([this.navigateTo]);
            });
        }
    }

    ngOnInit() {

        this.registerForm = new FormGroup({
            'id':new FormControl(this.Item.id),
            'name':new FormControl(this.Item.name,Validators.required),
            'phone':new FormControl(this.Item.phone,[Validators.required, Validators.minLength(9),Validators.pattern('^[0-9]+$')]),
            'email':new FormControl(this.Item.email,[Validators.required, Validators.email]),
            'password':new FormControl(this.Item.password,Validators.required),
            'website':new FormControl(this.Item.website),
            'description':new FormControl(this.Item.description),
            'sms_title':new FormControl(this.Item.sms_title,[Validators.required,,Validators.pattern('[A-Za-z]*')]),
        })


        this.logo.path =  this.imagepath+this.Item.image;


    }

    onFileChange(event, type) {
        let file = event.target.files[0];
        let reader = new FileReader();

        reader.onload = ev => {
            if (type === 'logo'){
                this.logo.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.logo.file =  file;
            }
        };
        reader.readAsDataURL(file);
    }
}
