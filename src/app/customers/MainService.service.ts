
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {SettingsService} from "../../settings/settings.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Observable} from 'rxjs/Observable';


//public this.ServerUrl = "";//http://www.tapper.co.il/salecar/laravel/public/api/";
@Injectable()

export class MainService
{
    public ServerUrl = "";
    public headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    public options = new RequestOptions({headers:this.headers});

    deleteModal: any;
    companyToDelete: any;

    public Items:any[]=[];

    constructor(private http:Http , private modalService: NgbModal,  settings:SettingsService)
    {
        this.ServerUrl = settings.ServerUrl;
    };

    GetItems(url:string)
    {
        let body = new FormData();
        body.append('uid', window.localStorage.identify);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.Items = data }).toPromise();
    }

    AddItem(url,data:any,File)
    {
        let body = new FormData();
        body.append("file", File);
        body.append("category", JSON.stringify(data));
        return this.http.post (this.ServerUrl + '' + url, body).map(res => res).do((data)=>{}).toPromise();
    }

    EditItem(url,Item,File)
    {
        console.log("IT : " , File)
        this.Items = Item;
        let body = new FormData();
        body.append("file", File);
        body.append("category", JSON.stringify(this.Items));
        //let body = 'category=' + JSON.stringify(this.Items);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{}).toPromise();
    }


    openDeleteModal(content,index)
    {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    }

    async deleteCompany()
    {
        this.deleteModal.close();
        console.log("Company To Delete : " , this.companyToDelete)
    }


    DeleteItem(url,Id)
    {
        let body = 'id=' + Id;
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(res => res.json()).do((data)=>{}).toPromise();
    }



};


