import {Component, OnInit, ElementRef, ViewChild, NgZone, ChangeDetectionStrategy} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormGroup,FormControl, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {DomSanitizer} from '@angular/platform-browser';
import * as Quill from 'quill';
import {ApiService} from '../../_services/api/api.service';



@Component({
    selector: 'app-edit',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './settings.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './settings.component.css']
})

export class SettingsComponent  implements OnInit {
    registerForm: FormGroup;
    public Id;
    public Items:any[]=[];
    public rowsNames:any[] = ['כותרת עמוד חברה','וידאו עמוד חברה'];
    public rows:any[] = ['company_title','company_video'];
    public Item;
    public host;
    public imagepath;
    public Change:boolean = false;
    logo = {path: null, file: null};
    errors: Array<any> = [];

    public title = "";

    /*

    form: FormGroup = this.fb.group({
        title: new FormControl('', Validators.required),
        website: new FormControl('', Validators.required),
        description: new FormControl('', Validators.required)
    });
    */

    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router, public settings: SettingsService,public fb: FormBuilder,private sanitizer: DomSanitizer,public api: ApiService) {
        this.route.params.subscribe(params => {
            this.Id = params['id'];
            //this.Item = this.service.Items[this.Id];
            this.host = settings.host;
            this.imagepath = settings.imagepath;
            //console.log("fkitchens " ,this.Item );
        });
    }

    async onSubmit(form:NgForm)
    {

        this.errors = [];

        let payload: any = {};
        payload.id = "1";
        payload.company_title = form.value.company_title;
        payload.company_description = form.value.company_description;
        payload.company_video = form.value.company_video;

        console.log(payload);
        this.api.sendPost('webUpdateCompanySettings', payload).subscribe(data => {
            alert ("עודכן בהצלחה");
        }, error => {
            alert ("שגיאה בעידכון יש לנסות שוב");
        });

    }

    ngOnInit() {


        this.registerForm = new FormGroup({
            'company_title':new FormControl("",Validators.required),
            'company_description':new FormControl("",Validators.required),
            'company_video':new FormControl("",Validators.required),
        })


        this.api.sendGet('webGetAppSettings').subscribe(data => {


            this.registerForm.setValue({
                company_title: data[0].company_title,
                company_description: data[0].company_description,
                company_video: data[0].company_video
            });

            const quill = new Quill('#editor-container', {
                modules: {toolbar: {container: '#toolbar-toolbar'}},
                theme: 'snow'
            });

            quill.on('text-change', () => {
                this.registerForm.controls.company_description.setValue(quill.root.innerHTML);
            });



            quill.clipboard.dangerouslyPasteHTML(data[0].company_description);

        }, error => {
            this.errors.push({message: "שגיאה בעידכון יש לנסות שוב"});
        });




    }

    onFileChange(event, type) {
        let file = event.target.files[0];
        let reader = new FileReader();

        reader.onload = ev => {
            if (type === 'logo'){
                this.logo.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.logo.file =  file;
            }
        };
        reader.readAsDataURL(file);
    }
}
