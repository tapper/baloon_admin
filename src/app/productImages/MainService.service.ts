
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {SettingsService} from "../../settings/settings.service";



//public this.ServerUrl = "";//http://www.tapper.co.il/salecar/laravel/public/api/";
@Injectable()

export class MainService
{
    public ServerUrl = "";
    public headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    public options = new RequestOptions({headers:this.headers});

    public Items:any[]=[];

    constructor(private http:Http ,  settings:SettingsService)
    {
        this.ServerUrl = settings.ServerUrl;
    };

    GetItems(url:string ,Id)
    {
        let body = new FormData();
        body.append('id', Id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.Items = data }).toPromise();
    }

    AddItem(url,Id,File)
    {
        let body = new FormData();
        body.append("file", File);
        body.append('id', Id);
        return this.http.post (this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{}).toPromise();
    }

    EditItem(url,Item,File)
    {
        console.log("IT : " , File , Item)
        this.Items = Item;
        let body = new FormData();
        body.append("file", File);
        body.append("category", JSON.stringify(this.Items));
        //let body = 'category=' + JSON.stringify(this.Items);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{}).toPromise();
    }

    DeleteItem(url,Id,Pid)
    {
        let body = new FormData();
        body.append("id", Id);
        body.append("pid", Pid);

        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{}).toPromise();
    }
};


