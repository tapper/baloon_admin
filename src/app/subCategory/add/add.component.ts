import {Component, OnInit, ElementRef, ChangeDetectionStrategy, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {Ng2UploaderModule} from 'ng2-uploader';
import {MainService} from "../MainService.service";

import {FileUploader, FileUploaderOptions} from 'ng2-file-upload/ng2-file-upload';


@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})
export class AddComponent {

    public navigateTo: string = '/subCategory/index';
    public paramsSub;
    public id: number;
    public Categories:any[]=[];
    public imageSrc: string = '';
    public isReady:Boolean = false;
    @ViewChild("fileInput") fileInput;
    public subId = ''

    public Product =
        {
            'category_id':'',
            'title':'',
            'show_on_main':0,
            'image':''
        }

    constructor(private route: ActivatedRoute, private http: Http, public service: MainService, public router: Router) {
        this.route.params.subscribe(params => {
            this.subId = params['id'];
        })

        this.service.GetAllItems('GetCategories',0).then((data: any) => {
            this.Categories = data;
            this.isReady = true;
            console.log("AddCat : " , data);
        });
    }

    onSubmit(form: NgForm) {

        let fi = this.fileInput.nativeElement;
        let fileToUpload;
        if (fi.files && fi.files[0]) {fileToUpload = fi.files[0];}
        console.log("Vl : " , form.value  , fileToUpload);

         if(form.value.title == "")
             alert ("הכנס שם תת קטגורייה")
        else if(form.value.category_id == "")
             alert ("חובה לבחור קטגורייה")
        else
         {
             this.service.AddItem('AddSubCategory',form.value,fileToUpload).then((data: any) => {
                 console.log("AddCompany : " , data);
                 this.router.navigate([this.navigateTo,{id:this.subId }]);
             });
         }
    }

    changeShowOnMain()
    {
        this.Product.show_on_main = this.Product.show_on_main==1?0:1;
        console.log(this.Product.show_on_main)
    }


}
