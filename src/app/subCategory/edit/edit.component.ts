import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";


const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './edit.component.css']
})

export class EditComponent  {

    public Id;
    public navigateTo:string = '/subCategory/index';
    public imageSrc: string = '';
    public Company:any[]=[];
    public Categories:any[]=[];
    public Kitchens:any[]=[];
    public Product;
    public changeImage;
    public host;
    public Change:boolean = false;
    @ViewChild("fileInput") fileInput;

    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router, public settings: SettingsService ) {
        this.route.params.subscribe(params => {
            this.Id = params['id'];

            this.service.GetItemById('GetSubCategoryById',this.Id).then((data: any) => {
                this.Product = data;
                console.log("AddSub : " , data);
            });

            this.service.GetAllItems('GetCategories',0).then((data: any) => {
                this.Categories = data;
                console.log("AddCat : " , data);
            });
        });

        this.host = settings.host
    }

    onSubmit(form:NgForm)
    {
        console.log("Edit1 : " , this.Product);
        let fi = this.fileInput.nativeElement;
        let fileToUpload;
        if (fi.files && fi.files[0]) {fileToUpload = fi.files[0]; console.log("fff : ",fileToUpload);}
        this.Product.change = this.Change;

        console.log("Edit2 : " , this.Product);
        this.service.EditItem('EditSubCategory',this.Product,fileToUpload).then((data: any) => {
            console.log("AddCompany : " , data);
            this.router.navigate([this.navigateTo]);
        });
    }

    onChange(event) {
        this.Change = true;
        var files = event.srcElement.files;
        this.changeImage = event.target.files[0];

        let reader = new FileReader();
        reader.onload = (e: any) => {
            this.changeImage = e.target.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }

    // handleInputChange(e) {
    //     var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    //
    //     var pattern = /image-*/;
    //     var reader = new FileReader();
    //
    //     if (!file.type.match(pattern)) {
    //         alert('invalid format');
    //         return;
    //     }
    //
    //     reader.onload = this._handleReaderLoaded.bind(this);
    //     reader.readAsDataURL(file);
    //     console.log("2 : " , reader)
    // }
    //
    // _handleReaderLoaded(e) {
    //     var reader = e.target;
    //     this.changeImage = reader.result;
    //     console.log("Cahnge")
    // }


    changeShowOnMain()
    {
        this.Product.show_on_main = this.Product.show_on_main==1?0:1;
    }

   //this.http.post('http://tapper.co.il/salecar/laravel/public/api/GetFile', formData, options)

}
