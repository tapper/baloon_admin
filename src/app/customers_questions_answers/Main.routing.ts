import {Routes} from '@angular/router';


import {EditComponent} from "./edit/edit.component";
import {AddComponent} from "./add/add.component";
import {IndexComponent} from "./index/index.component";

export const MaindRoutes: Routes = [{
    path: '',
    children: [{
        path: 'index',
        component: IndexComponent,
        data: {
            heading: 'לקוחות - שאלות ותשובות'
        }
    },{
        path: 'edit',
        component: EditComponent,
        data: {
            heading: 'עריכת שאלות ותשובות'
        }
    },{
        path: 'add',
        component: AddComponent,
        data: {
            heading: 'הוספת שאלות ותשובות'
        }
    }]
}];
