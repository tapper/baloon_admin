import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {EmployeeService} from "../employee.service";


const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './edit.component.css']
})

export class EditComponent  {

    public Id;
    public navigateTo:string = '';
    public imageSrc: string = '';
    public Company:any[]=[];
    public CompanyKitchens:any[]=[];
    public Kitchens:any[]=[];

    constructor(private route: ActivatedRoute,private http: Http, public service:EmployeeService , public router:Router) {
        this.route.params.subscribe(params => {
            this.Id = params['id'];
            this.Company = this.service.Companies[this.Id];
            this.Kitchens =  this.service.Kitchens
            this.CompanyKitchens = this.Company['kitchens'];
            this.navigateTo = '/employee/index;id='+this.Id;
            console.log("fkitchens " ,this.CompanyKitchens )
        });
    }

    onSubmit(form:NgForm)
    {
        console.log("Edit : " , this.Company);
        this.service.EditCompany('EditCompany',this.Company).then((data: any) => {
            console.log("AddCompany : " , data);
            this.router.navigate([this.navigateTo]);
        });
    }

    handleInputChange(e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

        var pattern = /image-*/;
        var reader = new FileReader();

        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }

        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        console.log("2 : " , reader)
    }

    _handleReaderLoaded(e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        console.log("3 : " , this.imageSrc)
    }

    checkKitchen(id)
    {
        let checked = 0;
        let kitchens1 = this.Company["kitchens"];
        for(let i=0;i<kitchens1.length; i++)
        {
            if(kitchens1[i]['resturant_id'] == this.Kitchens[id]['index'])
                checked = 1;
        }
        return checked;
    }

    changeKitchen(id)
    {
        let exsist = 0;
        console.log("fkitchens1 " ,this.CompanyKitchens )
        /*for(let i=0;i<this.CompanyKitchens.length; i++)
        {
            console.log(this.CompanyKitchens[i]['resturant_id']  + " : " + id)
            if(this.CompanyKitchens[i]['resturant_id'] == id)
            {
                console.log("splice" , this.CompanyKitchens);
                this.CompanyKitchens.splice(i, 1);
                exsist = 1;
                console.log("splice1" , this.CompanyKitchens);
            }
        }

        if(exsist == 0)
        {
            for(let j=0;j<this.Kitchens.length; j++)
            {
                console.log("Kitchen " , this.Kitchens[j]['index'] + " : "  + id );
                if(this.Kitchens[j]['index'] == id)
                {
                    this.CompanyKitchens.push(this.Kitchens[j])
                    console.log("splice2" , this.CompanyKitchens);
                }

            }
        }*/


    }

   //this.http.post('http://tapper.co.il/salecar/laravel/public/api/GetFile', formData, options)

}
